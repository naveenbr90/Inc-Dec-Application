terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

terraform {
  backend "local" {
    path = "terraform.tfstate"
  }
}


provider "aws" {
  region  = var.region
  profile = "default"
}

module "subnet" {
  source              = "./modules/sub-rt-nat"
  pri_subnet_az1_cidr = var.pri_subnet_az1_cidr
  pub_subnet_az1_cidr = var.pub_subnet_az1_cidr
  pub_subnet_az2_cidr = var.pub_subnet_az2_cidr
  main                = var.main

}

module "security_group" {
  source  = "./modules/securitygroup"
  sg_name = var.sg_name
  lb_name = var.lb_name
  main    = var.main
}



module "elb" {
  source                 = "./modules/elb"
  lb_name                = var.lb_name
  pub_elb_security_group = ["${module.security_group.pub_elb_security_group}"]
  pub_subnet_az1_cidr    = ["${module.subnet.pub_subnet_az1_cidr}"]
  pub_subnet_az2_cidr    = ["${module.subnet.pub_subnet_az2_cidr}"]
  main                   = var.main

}

module "asg" {
  source                 = "./modules/asg"
  ami_id                 = var.ami_id
  pri_ec2_security_group = ["${module.security_group.pri_ec2_security_group}"]
  pri_subnet_az1_cidr_id = ["${module.subnet.pri_subnet_az1_cidr}"]
  iam_instance_profile   = var.iam_instance_profile
  instance_type          = var.instance_type
  target_group_arn       = ["${module.elb.target_group_arn}"]

}


