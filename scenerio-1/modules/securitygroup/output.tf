output "pri_ec2_security_group" {
  value = aws_security_group.sg_ec2.id
}

output "pub_elb_security_group" {
  value = aws_security_group.sg_elb.id
}