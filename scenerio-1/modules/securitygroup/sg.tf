data "aws_vpc" "selected" {
  id = var.main
}

resource "aws_security_group" "sg_ec2" {
  name        = var.sg_name
  description = "Allow ELB inbound traffic"
  vpc_id      = var.main

  ingress {
    description = "TLS from VPC"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["${data.aws_vpc.selected.cidr_block}"]

  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name        = "ec2-sg"
    application = var.application
  }
}

resource "aws_security_group" "sg_elb" {
  name        = var.lb_name
  description = "Allow TLS inbound traffic"
  vpc_id      = var.main

  ingress {
    description = "TLS from VPC"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name        = "alb-sg"
    application = var.application
  }
}