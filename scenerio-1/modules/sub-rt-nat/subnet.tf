data "aws_availability_zones" "available_zones" {}



resource "aws_subnet" "private_app_subnet_az1" {
  vpc_id                  = var.main
  cidr_block              = var.pri_subnet_az1_cidr
  availability_zone       = data.aws_availability_zones.available_zones.names[0]
  map_public_ip_on_launch = false

  tags = {
    Name        = var.pri_subnet_az1_cidr-name
    application = var.application
  }
}


resource "aws_route_table" "myroute" {
  vpc_id = var.main

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = var.mynat_id

  }
  tags = {
    Name        = var.route-name
    application = var.application
  }
}

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.private_app_subnet_az1.id
  route_table_id = aws_route_table.myroute.id
}

# create public subnet az1
resource "aws_subnet" "public_subnet_az1" {
  vpc_id                  = var.main
  cidr_block              = var.pub_subnet_az1_cidr
  availability_zone       = data.aws_availability_zones.available_zones.names[0]
  map_public_ip_on_launch = true
  tags = {
    Name        = var.pub_subnet_az1_cidr-name
    application = var.application
  }
}

# create public subnet az2
resource "aws_subnet" "public_subnet_az2" {
  vpc_id                  = var.main
  cidr_block              = var.pub_subnet_az2_cidr
  availability_zone       = data.aws_availability_zones.available_zones.names[1]
  map_public_ip_on_launch = true

  tags = {
    Name        = var.pub_subnet_az2_cidr-name
    application = var.application
  }
}
