output "pri_subnet_az1_cidr" {
  value = aws_subnet.private_app_subnet_az1.id
}

output "pub_subnet_az1_cidr" {
  value = aws_subnet.public_subnet_az1.id

}

output "pub_subnet_az2_cidr" {
  value = aws_subnet.public_subnet_az2.id
}