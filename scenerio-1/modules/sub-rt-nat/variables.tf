#values passed from tfvars

variable "pri_subnet_az1_cidr" {}
variable "pub_subnet_az1_cidr" {}
variable "pub_subnet_az2_cidr" {}
variable "main" {}

variable "mynat_id" {
  default = "nat-04ee97920605cd9dd"
}
variable "eip" {
  default = "13.127.128.156"
}

# Variables with default values

variable "pri_subnet_az1_cidr-name" {
  default = "pri-sub1"
}

variable "mynat-name" {
  default = "mynat"
}

variable "application" {
  default = "Inc-Dec-App"
}

variable "route-name" {
  default = "MyRoute-Nginx"
}

variable "pub_subnet_az1_cidr-name" {
  default = "pub_sub1"
}

variable "pub_subnet_az2_cidr-name" {
  default = "pub_sub2"
}

variable "availability_zone" {
  default = "ap-south-1a"
}