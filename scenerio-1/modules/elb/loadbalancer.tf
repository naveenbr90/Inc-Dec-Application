# Application Load balancer

resource "aws_lb" "alb_frontend" {
  name               = var.lb_name
  internal           = false
  load_balancer_type = "application"
  security_groups    = var.pub_elb_security_group
  subnet_mapping {
    subnet_id = flatten(var.pub_subnet_az1_cidr)[0]
  }

  subnet_mapping {
    subnet_id = flatten(var.pub_subnet_az2_cidr)[0]
  }

  #subnets            = [for subnet in aws_subnet.public : subnet.id]

  enable_deletion_protection = false

  tags = {
    application = var.application
  }
}

resource "aws_lb_listener" "front_end" {
  load_balancer_arn = aws_lb.alb_frontend.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.target_group.arn
  }
}


resource "aws_lb_target_group" "target_group" {
  name        = var.tg_name
  port        = var.port
  protocol    = "HTTP"
  vpc_id      = var.main
  target_type = "instance"
  health_check {
    enabled             = true
    healthy_threshold   = 5
    interval            = 5
    matcher             = "200"
    path                = "/"
    port                = "80"
    protocol            = "HTTP"
    timeout             = 3
    unhealthy_threshold = 5
  }

}





