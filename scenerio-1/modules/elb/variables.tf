variable "lb_name" {
  default = "nginx-lb"
}

variable "application" {
  default = "Inc-Dec-App"
}

variable "pub_elb_security_group" {}

variable "pub_subnet_az1_cidr" {}

variable "pub_subnet_az2_cidr" {}

variable "tg_name" {
  default = "tg-nginx"
}

variable "port" {
  default = "80"
}

variable "main" {

}

##variable "aws_instance" {
  
#}

