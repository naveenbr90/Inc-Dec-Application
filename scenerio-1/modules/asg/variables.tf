variable "auto_scalling_name" {
  default = "Nginx-ASG"
}

variable "launch_temp_name" {
  default = "nginx-lt"
}
variable "instance_type" {
  default = "t2.micro"
}
variable "ami_id" {

}

variable "pri_ec2_security_group" {

}
variable "target_group_arn" {
  
}
variable "availability_zone" {
  default = "ap-south-1a"
}

variable "availability_zones" {
  default = "ap-south-1a"
}

variable "application" {
  default = "Inc-Dec-App"
}

variable "pri_subnet_az1_cidr_id" {

}

variable "iam_instance_profile" {

}