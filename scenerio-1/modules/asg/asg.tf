data "aws_availability_zones" "available_zones" {}


resource "aws_launch_template" "launch_temp" {
  name = "foo"

  iam_instance_profile {
    name = var.iam_instance_profile
  }

  image_id = var.ami_id

  instance_initiated_shutdown_behavior = "terminate"

  instance_type = var.instance_type

  metadata_options {
    http_endpoint               = "enabled"
    http_tokens                 = "required"
    http_put_response_hop_limit = 1
    instance_metadata_tags      = "enabled"
  }

  monitoring {
    enabled = false
  }


  placement {
    availability_zone = var.availability_zone
  }

  vpc_security_group_ids = var.pri_ec2_security_group

  tag_specifications {
    resource_type = "instance"

    tags = {
      Name = var.application
    }
  }

  user_data = filebase64("user-data.sh")
}

resource "aws_autoscaling_group" "autoscalling" {
  name                = var.auto_scalling_name
  max_size            = 3
  min_size            = 1
  health_check_type   = "EC2"
  desired_capacity    = 1
  vpc_zone_identifier = var.pri_subnet_az1_cidr_id
  target_group_arns = var.target_group_arn

  launch_template {
    id      = aws_launch_template.launch_temp.id
    version = "$Latest"
  }
}
