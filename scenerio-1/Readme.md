## Inc Dec App

Increment & Decrement counter App - That is used to increase and decrease counter by simple click from browser

## Description
Running Nginx Server in to server public web content behind ALB in SG region , S3 Private bucket content need to be downloaded at every reboot, Deploying Increment and decrement counter code and creating a Gitlab pipeline for to push the code to s3 from private bucket and Instance refresh of the auto scalling group.

## About Repo

This Repo will have Infrastructure as a code for AWS Cloud resource provision as requested in Architecture daigram.
 
Terraform:

Assuming we using existing VPC and below remaining resources are created using the Terraform code.
* Subnet - Private & Public Subnet
* NAT
* Rout Table & Association for Private subnet
* EC2 Instance , IAM Role , ALB , Autoscalling Group

GitLab Pipeline:

* Pipeline Job that will build the artifacts from Git repo branch
* Pushing the content to S3 Private Bucket
* AWS Autoscalling group instance refresh

## Usage

cd existing_repo
git remote add origin https://gitlab.com/imnaveen/nsearch.git
git branch -M main
git push -uf origin main

# Local setup & Pre-Req

* AWS Cli
* Terraform
* Gitlab runner 

Please follow standard process for local setup..!

Deliverable:

1. Restrict inbound access to both public ALB and Server Fleet A to only allow on port
80/TCP
image.png
image.png

2. Install Nginx on Amazon Linux 2, Server Fleet A of EC2

## Further Improvments 

1. Enabling HTTPS connection with secure port for ALB -- To improve the security of the site because nonsecure port will cause potential risk.

2. Enabling Bucket versioning for artifacts

3. Maintaining terraform state file with db locking

4. To have resource outside default VPC.

5. Same its simple httml application then can be hosted in s3 and serve the content via cloudfront.

## Deliverable:
● Restrict inbound access to both public ALB and Server Fleet A to only allow on port
80/TCP -- > Yes Terraform will enable only port 80.

● Install Nginx on Amazon Linux 2, Server Fleet A of EC2  --> Deployed Nginx

● Able to view Incremental and Decremental Counter webpage from public internet
served by Server Fleet A (Screenshot required for both the original page and
modified uppercase text page) -- Please find the screenshot attached <>

● Describe in a README file, what further improvements to above architecture and
setup would you recommend and why.--> Described as per my best of knowledge

● Share the Gitlab pipeline with your interviewer --> Yes

● Push your terraform, web codes and gitlab-ci.yml file to your gitlab repo to be shared
with your interviewer --> yes 
 
