variable "region" {
  default = "ap-south-1"
}
variable "ami_id" {
  default = "ami-0d514c86514fc07f5"
}
variable "main" {
  default = "vpc-02853ea039ee064b0"
}
variable "instance_type" {
  default = "t2.micro"
}

variable "pri_subnet_az1_cidr" {
  default = "173.31.55.0/24"
}

variable "pub_subnet_az1_cidr" {
  default = "173.31.66.0/24"

}

variable "pub_subnet_az2_cidr" {
  default = "173.31.66.0/24"

}

variable "sg_name" {
  default = "nginx-security-ec2-group"
}

variable "lb_name" {
  default = "nginx-elb"
}

variable "eib" {
  default = "13.127.158.156"
}

variable "iam_instance_profile" {
  default = "ec2-role"
}


