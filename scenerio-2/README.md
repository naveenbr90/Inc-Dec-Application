## Inc Dec App

Increment & Decrement counter App - That is used to increase and decrease counter by simple click from browser

## Assumtions on the requirment :
As per requirment need to create an application which can capture the input on increment and decrement counter from browser and insert into MySQL Database.

So for this i have used Node JS , Front end will be same Increment & decrement code which i have used in Nginx for scenerio 1.

To make the database connection from application i have created a small app which will make a db connection and input data.

When user clicks the +/- the data should be captured for each click that has been made.

To make anyone accessable i have created a Docker image which can deployed anywhere ..!

Node JS and MSSQL are running in two different pod's.

For Node JS application DB details are passed in environment variables.

MSSQL Resource is monitored with Promethenus and Grafana using sidecar

## Local setup:

cd existing_repo
git remote add origin https://gitlab.com/imnaveen/nsearch.git
git branch -M main
git push -uf origin main

# Local setup & Pre-Req

* Docker
* Minikube
* Helm

# Please execute below steps for local setup

cd <CheckoutRootDirectory>/nsearch/inc-dec-app/


kubectl create secret generic regcred --from-file=.dockerconfigjson=~/.docker/config.json --type=kubernetes.io/dockerconfigjson   // To pull the image from Docker Hub.

kubectl create namespace monitoring-app  // This is the namespace that will be tagged for all the resource that we will be creating , if this is changed then need to update in .yaml file as well..!

## Next comes the MSSQL deployment with Monitoring enabled.


1. kubectl apply -f mssql-secrets.yaml  // Need to create a secret service for DB password instead to hard coded in any files.

2. Kubectl apply -f mssql-monitoring-prom-grafana.yaml  // This yaml file will create a stateful set for mssql , Promothenus , Grafana , DB and table creation..!

## Using Helm charts we can create a monitoring for promrthenus and Grafana dashboard 

3. helm repo add prometheus-community https://prometheus-community.github.io/helm-charts  // to add git repo to helm

4. helm repo update

5. helm install prometheus prometheus-community/kube-prometheus-stack --namespace monitoring-app // This will create a resources for monitoring.

## Once the installing is completed we have to enable the port forwarding for promothenus and Grafana..!

6. kubectl get pods -n monitoring // to see pods created under namepsace monitoring-app

7. kubectl get svc -n monitoring  // To see service created under namespace monitoring-app

## To forward local port to cluster port need to run below commands..!

8. kubectl port-forward svc/prometheus-kube-prometheus-prometheus -n monitoring 9090 // Promethenus

9. kubectl port-forward svc/prometheus-grafana -n monitoring 80 // Grafana

## Below yaml file will deploy Node JS POD with docker image specified which has Inc-Dec-App

10. Before deploying application pod capture the MSSQL pod IP and update in environmental variable in node.yaml and deploy..!

11. kubectl.exe get pods -n monitoring -o wide  // To capture IP and update the environmental variable

12. kubectl apply -f node-inc-dec-app.yml

## Creating Service for node application

13. kubectl apply -f node-app-service.yml

14. kubectl expose deployment <pod-Name> -n monitoring

15. kubectl.exe port-forward service/node-app-service 5000 -n monitoring  // To forward port from local to pod

http://127.0.0.1/5000

## Deliverable:

1.  Make use of scenario 1 code and display your <your first name> using an environment variable supplied to the container. --> In html page we cannont refer environmental variable since it runs on client side, but i have have passed environment variable and used in container to print the name in log..!

2. Document in a README.md, the assumptions made when creating this site and provide us a script to spin up the Application using local kubernetes
(you can include any troubleshooting steps if any). --> Application is deployed in local using kubernetes cluster

3. Ensure that the Incremental and Decremental Counter is able to connect to Mysql server. No code change is required from scenario 1. --> Yes MSSQL is deployed and able to connect from application successfull

4.  Monitor the resources using Prometheus and Grafana - Installed Prometheus and Grafana

5. Push your code to your gitlab repo to be shared with your interviewer. -- Done
